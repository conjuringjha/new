import React from 'react';
import { Button, View, Text, TextInput, Switch, StyleSheet } from 'react-native';
import { createStackNavigator } from 'react-navigation'; // Version can be specified in package.json
import Geolocation from 'react-native-geolocation-service';

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Button
          title="Add New Outlet"
          onPress={() => this.props.navigation.navigate('Details')}
        />
      </View>
    );
  }
}

class DetailsScreen extends React.Component {
	constructor(props) {
    super(props);
    this.state = { latitude: 34.34,
					longitude: 45.35,
					Area: 'useless Area',
					City: 'useless City',
					State: 'useless State',
					Pin: 'useless Pin' ,
					Name: 'Outlet Name',
	};

  }
  componentDidMount() {
    Geolocation.getCurrentPosition(
      (position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: null,
        });
      },
      (error) => this.setState({ error: error.message }),
	  { enableHighAccuracy: false, timeout: 30000, maximumAge: 10000 },
	  
    );
	
	
}
getCoordinates() {
        
        Geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                  });
            },
            (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 30000, maximumAge: 10000 }
);}

  render() {
    return (
	<View>
	<Text>Latitude</Text>
      <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
		keyboardType='numeric'
        onChangeText={(text) => this.setState({text})}
        value={this.state.latitude.toString()}
      />
	  <Text>Longitude</Text>
	  <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
		keyboardType='numeric'
        onChangeText={(text) => this.setState({text})}
        value={this.state.longitude.toString()}
      />
	  <Text>Area</Text>
	  <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
        onChangeText={(text) => this.setState({text})}
        value={this.state.Area}
      />
	  <Text>City</Text>
	  <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
        onChangeText={(text) => this.setState({text})}
        value={this.state.City}
      />
	  <Text>State</Text>
	  <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
        onChangeText={(text) => this.setState({text})}
        value={this.state.State}
      />
	  <Text>Pin</Text>
	  <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
        onChangeText={(text) => this.setState({text})}
        value={this.state.Pin}
      />
	  <Text>OutletName</Text>
	  <TextInput
        style={{height: 40, borderColor: 'gray', borderWidth: 1}}
        onChangeText={(text) => this.setState({text})}
        value={this.state.Name}
      />
	  <Button
          title="Update"
          onPress={() => this.getCoordinates}
        />
	  <Button
         title="Submit"
         color="#841584"
       />
	  </View>

    )
  }
  

}

const RootStack = createStackNavigator(
  {
    Home: HomeScreen,
    Details: DetailsScreen,
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}

const styles = StyleSheet.create ({
   container: {
      flex: 1,
      alignItems: 'center',
      marginTop: 50
   },
   boldText: {
      fontSize: 30,
      color: 'red',
   }
})